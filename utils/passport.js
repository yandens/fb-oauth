const passport = require('passport')
const FacebookStrategy = require('passport-facebook').Strategy
const config = require('../config')

// create session
passport.serializeUser((user, cb) => {
  cb(null, user)
})

passport.deserializeUser((obj, cb) => {
  cb(null, obj)
})

passport.use(new FacebookStrategy(
  {
    clientID: config.clientID,
    clientSecret: config.clientSecret,
    callbackURL: config.callbackURL
  }, (accessToken, refreshToken, profile, done) => {
    return done(null, profile)
  }
))

module.exports = passport
