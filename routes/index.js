const router = require('express').Router()
const passport = require('passport')
const middleware = require('../middlewares/auth')

// index
router.get('/', (req, res) => {
  res.render('index')
})

// profile
router.get('/profile', middleware.isLogin, (req, res) => {
  res.render('profile', {
    user: req.user
  })
})

// error
router.get('/error', middleware.isLogin, (req, res) => {
  res.render('error')
})

// login with facebook
router.get('/auth/facebook', passport.authenticate('facebook', {
  scope: ['public_profile', 'email']
}))

router.get('/auth/facebook/callback', passport.authenticate('facebook', {
  successRedirect: '/profile',
  failureRedirect: '/error'
}))

// logout
router.get('/logout', (req, res, next) => {
  try {
    res.logout()
    res.redirect('/')
  } catch (err) {
    next(err)
  }
})

module.exports = router
